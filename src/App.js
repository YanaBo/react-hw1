import React, { Component } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import './styles.scss';


class App extends Component {
  state = {
    isOpenModalOne: false,
    isOpenModalTwo: false
  };

  handleOpenModalOne = () => {
    this.setState({ isOpenModalOne: true, isOpenModalTwo: false })
  };

  handleCloseModalOne = () => {
    this.setState({ isOpenModalOne: false })
  };

  handleOpenModalTwo = () => {
    this.setState({ isOpenModalOne: false, isOpenModalTwo: true })
  };

  handleCloseModalTwo = () => {
    this.setState({ isOpenModalTwo: false })
  };

  render() {
    return (
        <div className="App">
          <Button
              onOpenModal={this.handleOpenModalOne}
              text="Open First Modal"
              backgroundColor="pink"
          />
          <Button
              onOpenModal={this.handleOpenModalTwo}
              text="Open Second Modal"
              backgroundColor="paleturquoise"
          />
          <Modal
              onCloseModal={this.handleCloseModalOne}
              isOpen={this.state.isOpenModalOne}
              header='Do you want to delete this file?'
              closeButton={true}
              text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it? "
              actions={[
                <button key='button1'>Ok</button>,
                <button key='button2'>Cancel</button>
              ]}
          />
          <Modal
              onCloseModal={this.handleCloseModalTwo}
              isOpen={this.state.isOpenModalTwo}
              header='Registration form'
              closeButton={true}
              text="Press button for quick registration"
              actions={[
                <button key='button1'>Register</button>,
                <button key='button2'>Cancel</button>
              ]}
          />
        </div>
    );
  }
}

export default App;
