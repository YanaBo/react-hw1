import React, { Component } from "react";
import PropTypes from 'prop-types';
import './styles.scss';

class Button extends Component {

 static propTypes = {
   onOpenModal: PropTypes.func,
   text: PropTypes.string,
   backgroundColor: PropTypes.string
 };

  render() {
    return (
        <button
            onClick={ this.props.onOpenModal }
            className='button'
            style={{ 'background': this.props.backgroundColor }}>
          { this.props.text }
        </button>
    );
  }
}

export default Button;