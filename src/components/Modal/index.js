import React, { Component } from "react";
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';
import { fadeIn } from 'react-animations';
import './styles.scss';

class Modal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool,
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.arrayOf(PropTypes.element),
    closeButton: PropTypes.bool,
    onCloseModal: PropTypes.func
  };

  render() {

    const { isOpen, header, text, actions, closeButton, onCloseModal } = this.props;
    const FadeIn = styled.div`animation: 0.5s ${keyframes`${fadeIn}`}`;

    if (!isOpen) return null;

    return (
        <FadeIn>
          <div className="modal-wrapper">
            <div className="modal-window">
              <div className="head">
                <h2 className="header">{header}</h2>
                {closeButton && <button className="close" onClick={onCloseModal}>x</button>}
              </div>
              <div className="modal-text">
                <p>{text}</p>
              </div>
              <div className="footer">
                {actions}
              </div>
            </div>
            <div className="overlay" onClick={onCloseModal}/>
          </div>
        </FadeIn>
    )
  }
}

export default Modal;